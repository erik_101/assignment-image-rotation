#ifndef LAB3___2_ROTATOR_H
#define LAB3___2_ROTATOR_H

#include "image.h"
#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

struct image rotate(struct image source);

#endif //LAB3___2_ROTATOR_H
