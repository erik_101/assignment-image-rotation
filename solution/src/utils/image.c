#include "../../include/image.h"

struct image get_image(uint32_t width, uint32_t height, struct pixel *data) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = data
    };
}

struct image create_image(uint32_t width, uint32_t height) {

    struct pixel *pixels = malloc(sizeof(struct pixel) * width * height);

    return get_image(width, height, pixels);
}

void destroy_image(struct image* image) {
    free(image->data);
}
