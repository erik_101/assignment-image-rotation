#include "../include/file_manager.h"
#include "../include/bmp_handler.h"
#include "../include/rotator.h"
#include <stdlib.h>

int main(int argc, char **argv) {

    if (argc < 3) {
        fprintf(stderr, "Invalid number of arguments!\n"
                        "Expected: <source-image>\n"
                        "          <transformed-image>");
        return -1;
    }

    FILE *input_file;

    if (open_file(&input_file, argv[1], "r") != OPEN_SUCCESS) {
        fprintf(stderr, "Unable to open input file for read!");
        return -1;
    }

    struct image source_image = {0};

    if (from_bmp(input_file, &source_image) != READ_SUCCESS) {
        fprintf(stderr, "Unable to read image from input file!");
        return -1;
    }

    if (close_file(input_file) != CLOSE_SUCCESS) {
        fprintf(stderr, "Warning: Unable to close input file!");
    }

    struct image rotated_image = rotate(source_image);

    FILE *output_file;

    if (open_file(&output_file, argv[2], "w") != OPEN_SUCCESS) {
        fprintf(stderr, "Unable to open output file for write!");
        return -1;
    }

    if (to_bmp(output_file, &rotated_image) != WRITE_SUCCESS) {
        fprintf(stderr, "Unable to write image to output file!");
        return -1;
    }

    if (close_file(output_file) != CLOSE_SUCCESS) {
        fprintf(stderr, "Warning: Unable to close output file!");
    }

    destroy_image(&source_image);
    destroy_image(&rotated_image);

    return 0;
}